//
//  AddCategoryViewController.m
//  ShoppingList
//
//  Created by Aquiles Alfaro on 12/16/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "AddCategoryViewController.h"
#import "Category+CoreDataClass.h"

@interface AddCategoryViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UITextField *txtCategory;

@end

@implementation AddCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    context = appDelegate.persistentContainer.viewContext;
    
    if (self.aCategory){
     
        self.txtCategory.text = [self.aCategory valueForKey:@"categoryItem"];
        
    }
        
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)dissmissKeyboard:(UITextField *)sender {

}
- (IBAction)saveCategory:(UIButton *)sender {
    if (self.aCategory){
        [self.aCategory setValue:self.txtCategory.text forKey:@"categoryItem"];
    }else{
    
    Category *myCategory = [[Category alloc]initWithContext:context];
    
    [myCategory setValue:self.txtCategory.text forKey:@"categoryItem"];
    
    }
    
    // zero out the fields
    self.txtCategory.text = @"";
    
    // commit
    NSError *error = nil;
    if(![context save:&error]){
        NSLog(@"%@ %@", error, [error localizedDescription]);
        
    }
    
    // Dismiss the view
    [self.navigationController popViewControllerAnimated:YES];
    
    
}


@end
