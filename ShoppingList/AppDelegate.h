//
//  AppDelegate.h
//  ShoppingList
//
//  Created by Aquiles Alfaro on 12/16/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (strong, nonatomic)NSManagedObjectContext *context;

- (void)saveContext;


@end

