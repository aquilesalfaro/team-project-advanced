//
//  CategoryTableViewController.h
//  ShoppingList
//
//  Created by Aquiles Alfaro on 12/16/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface CategoryTableViewController : UITableViewController{
    AppDelegate *appDelegate;
    NSManagedObjectContext * context;
}

@end
