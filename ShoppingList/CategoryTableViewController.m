//
//  CategoryTableViewController.m
//  ShoppingList
//
//  Created by Aquiles Alfaro on 12/16/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "CategoryTableViewController.h"
#import "AddCategoryViewController.h"

@interface CategoryTableViewController ()
@property (strong) NSMutableArray *categories;

@end

@implementation CategoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate*) [[UIApplication sharedApplication]delegate];
    context = appDelegate.persistentContainer.viewContext;
    
          
    }
    
-(void)viewDidAppear:(BOOL)animated{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Category"];
    self.categories = [[context executeFetchRequest:fetchRequest error:nil]mutableCopy];
    
    NSArray * sortCat;
    sortCat = self.categories;
    
    NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:@"categoryItem" ascending:YES];
    sortCat = [sortCat sortedArrayUsingDescriptors:@[sd]];
    
    self.categories = [NSMutableArray arrayWithArray:sortCat];
    
    
    
    // refresh the table view
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.categories count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CatCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSManagedObjectModel *aCategory = [self.categories objectAtIndex:indexPath.row];
    
    
    
    
    [cell.textLabel setText:[NSString stringWithFormat:@"%@", [aCategory valueForKey:@"categoryItem"]]];
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [context deleteObject:[self.categories objectAtIndex:indexPath.row]];
        
        [self.categories removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        
  
        
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"EditCat"]){
        NSManagedObjectModel *selectedCategory = [self.categories objectAtIndex:[[self.tableView indexPathForSelectedRow]row]];
        AddCategoryViewController *updateCategory = segue.destinationViewController;
        updateCategory.aCategory = selectedCategory;
        
    }
    
    
}


@end
